import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.aris.checklistapp',
  appName: 'Checklist App',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
